import urllib.request, json 
# "http://badburrito.s2ph4xbvfr.us-east-2.elasticbeanstalk.com/list"
#[{"Employee Name":"Curtis, Ashley","Sum of Missing TS":16,"Burritos":16,"Rank":1},{"Employee Name":"Cherian, Nikhil","Sum of Missing TS":15,"Burritos":15,"Rank":2},{"Employee Name":"Chatterjee, Abhishek","Sum of Missing TS":14,"Burritos":14,"Rank":3},{"Employee Name":"Chopra, Shorye","Sum of Missing TS":14,"Burritos":14,"Rank":4},{"Employee Name":"Varghese, Shyam","Sum of Missing TS":13,"Burritos":13,"Rank":5},{"Employee Name":"Petroski, Deni","Sum of Missing TS":13,"Burritos":13,"Rank":6},{"Employee Name":"Straube, Dayane","Sum of Missing TS":11,"Burritos":11,"Rank":7},{"Employee Name":"Matters, Rebecca","Sum of Missing TS":11,"Burritos":11,"Rank":8},{"Employee Name":"Wright, James","Sum of Missing TS":10,"Burritos":10,"Rank":9},{"Employee Name":"Barden, Stephen","Sum of Missing TS":10,"Burritos":10,"Rank":10},{"Employee Name":"Gourishetty, Babitha","Sum of Missing TS":9,"Burritos":9,"Rank":11},{"Employee Name":"Jordan, Scott","Sum of Missing TS":9,"Burritos":9,"Rank":12},{"Employee Name":"Penugonda, Prasanna","Sum of Missing TS":8,"Burritos":8,"Rank":13},{"Employee Name":"Jaiswal, Shweta","Sum of Missing TS":8,"Burritos":8,"Rank":14},{"Employee Name":"Rawal, Umesh","Sum of Missing TS":7,"Burritos":7,"Rank":15},{"Employee Name":"Subramaniam, Gajendran","Sum of Missing TS":6,"Burritos":6,"Rank":16},{"Employee Name":"Hargreaves, Geoff","Sum of Missing TS":5,"Burritos":5,"Rank":17},{"Employee Name":"Mangat, Virinder","Sum of Missing TS":4,"Burritos":4,"Rank":18},{"Employee Name":"Bars, Peter","Sum of Missing TS":4,"Burritos":4,"Rank":19},{"Employee Name":"Jamieson, Mark","Sum of Missing TS":4,"Burritos":4,"Rank":20},{"Employee Name":"Deshmukh, Nakul","Sum of Missing TS":4,"Burritos":4,"Rank":21},{"Employee Name":"Dodd, Erica","Sum of Missing TS":4,"Burritos":4,"Rank":22},{"Employee Name":"Gadepalli, Anu","Sum of Missing TS":3,"Burritos":3,"Rank":23},{"Employee Name":"Chauhan, Mamta","Sum of Missing TS":3,"Burritos":3,"Rank":24},{"Employee Name":"Anand, Heena","Sum of Missing TS":3,"Burritos":3,"Rank":25},{"Employee Name":"Lawless, Janan","Sum of Missing TS":3,"Burritos":3,"Rank":26},{"Employee Name":"El-Shenawy, Sharif","Sum of Missing TS":3,"Burritos":3,"Rank":27},{"Employee Name":"Johnson, Olivia","Sum of Missing TS":2,"Burritos":2,"Rank":28},{"Employee Name":"Carmeli, Erez","Sum of Missing TS":2,"Burritos":2,"Rank":29},{"Employee Name":"Gatt, Chris","Sum of Missing TS":2,"Burritos":2,"Rank":30},{"Employee Name":"Prabhu, Shyama","Sum of Missing TS":1,"Burritos":1,"Rank":31},{"Employee Name":"Dsouza, Tanya","Sum of Missing TS":1,"Burritos":1,"Rank":32},{"Employee Name":"Gunn, Gordon","Sum of Missing TS":1,"Burritos":1,"Rank":33},{"Employee Name":"Lakoumentas, Dimi","Sum of Missing TS":0,"Burritos":0,"Rank":34},{"Employee Name":"Devlin, Joel","Sum of Missing TS":0,"Burritos":0,"Rank":35},{"Employee Name":"Kahler, Christopher","Sum of Missing TS":0,"Burritos":0,"Rank":36},{"Employee Name":"Maxwell, Melissa","Sum of Missing TS":0,"Burritos":0,"Rank":37}]
def extract(URL):
    with urllib.request.urlopen(URL) as url:
        data = json.loads(url.read().decode())
        data_extract = []
        data_burrito = []
        for element in data:
            # name = print(element)
            # count = print(data[element]["Burritos"])
            element["name_clean"] = element["Employee Name"].replace(",","")
            element["name_clean_rev"] = (" ".join(reversed(element["name_clean"].split(" "))))
            data_extract.append((" ".join(reversed(element["name_clean"].split(" ")))))
            # data_burrito.append(element["Burritos"])
            data_burrito.append(element)

            # data_extract.append((" ".join(reversed(element["name_clean"].split(" ")))))
    return data, data_extract, data_burrito

with urllib.request.urlopen("http://badburrito1.s2ph4xbvfr.us-east-2.elasticbeanstalk.com/list") as url:
    data = json.loads(url.read().decode())
    data_extract = []
    data_burrito = []
    for element in data:
        # name = print(element["Employee Name"])
        # count = print(data[element]["Burritos"])
        # element["Employee Name"] = element["Employee Name"]
        element["name_clean"] = element["Employee Name"].replace(",","")
        element["name_clean_rev"] = (" ".join(reversed(element["name_clean"].split(" "))))
        data_extract.append((" ".join(reversed(element["name_clean"].split(" ")))))
        data_burrito.append(element)
    print(data_burrito)
    print(data_burrito[1]["name_clean"]&["Burritos"])


    # print(data_burrito)
        # print(data_extract)
        # print(element["name_clean"])
        # print(element["Employee Name"])
    # print(data)
    # print(data[1]["name_clean_rev"])
    # print(data.keys())
    