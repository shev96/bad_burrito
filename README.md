**Bad Burrito Bot**
==========================

Requirements
==========
* python

Usage of BurritoBot
=======

* To run BurritoBot: _python deployburrito.py_

While in a Slack channel with BurritoBot

* To check burrito count: _@BurritoBot burrito_
