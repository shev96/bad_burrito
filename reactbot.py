import os
import time
import re
from slackclient import SlackClient

# SLACK_BOT_TOKEN='xoxb-528667146421-537045169862-VOMrlBMCHe4vPK1Gw7RbI3ki'

# instantiate Slack client
# slack_client = SlackClient(os.environ.get('xoxb-528667146421-537045169862-VOMrlBMCHe4vPK1Gw7RbI3ki'))
slack_client = SlackClient('xoxb-528667146421-537045169862-VOMrlBMCHe4vPK1Gw7RbI3ki')
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 1 # 1 second delay between reading from RTM
EXAMPLE_COMMAND = "burrito"
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

###########################
def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get('ok'):
        return channels_call['channels']
    return None

def list_users():
    users_call = slack_client.api_call("users.list")
    if users_call.get('ok'):
        return users_call['members']
    return None

def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == starterbot_id:
                return message, event["channel"], event["user"]
    return None, None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel, user_ask,users):
    """
        Executes bot command if the command is known
    """
    users = list_users()
    csv_data, csv_array,csv_dict = extract("http://badburrito.s2ph4xbvfr.us-east-2.elasticbeanstalk.com/list")
    flag = 0
    for u in users:
        if u["id"] == user_ask:
            name_found = u["profile"]["display_name"]
            try: 
                burritos = csv_dict[name_found]
            except:
                flag = 1
    
    # Default response is help text for the user
    default_response = "Not sure what you mean. Try *{}*.".format(EXAMPLE_COMMAND)

    # Finds and executes the given command, filling in response
    response = None
    # This is where you start to implement more commands!
    if command.startswith(EXAMPLE_COMMAND):
        # response = "You have X amount of burritos! :burrito: "
        if (flag):
            response = "You are not in the burrito database."
        else:
            response = "You have {} burritos! :burrito:".format(burritos)

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response or default_response
    )

###########################
import schedule
import time

               
# from retrieve import extract
from reactget import extract
# csv_data, csv_data_extract, csv_burrito = extract("http://badburrito1.s2ph4xbvfr.us-east-2.elasticbeanstalk.com/list")
csv_data, csv_array,csv_dict = extract("http://badburrito.s2ph4xbvfr.us-east-2.elasticbeanstalk.com/list")

if __name__ == "__main__":

    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        channels = list_channels()
        users = list_users()
        # print(users[1]["profile"]["email"])
        # print(csv_data[1])

        while True:
            # schedule.run_pending()
            command, channel, user_ask = parse_bot_commands(slack_client.rtm_read())
            # for u in users:
            #     if u["id"] == user_ask:
            #         name_found = u["profile"]["display_name"]
            if command:
                # handle_command(command, channel,csv_dict[name_found])
                handle_command(command, channel,user_ask,users)
            time.sleep(RTM_READ_DELAY)
            # time.sleep(1)
    else:
        print("Connection failed. Exception traceback printed above.")