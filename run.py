from flask import Flask
from flask_restful import Api, Resource, reqparse
from Script import data
import global_variables as g0
app=Flask(__name__)
api=Api(app)
data()
users = g0.df
users = users[users['Sum of Missing TS'] > 0]




class User(Resource):
    def get(self, name):
        for user in users:
            if(name == user["name"]):
                return user, 200
        return "User not found", 404


api.add_resource(User, "/user/<string:name>")
