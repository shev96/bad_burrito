import os
import time
import re
from slackclient import SlackClient
from reactget import extract_raw, extract

# SLACK_BOT_TOKEN='xoxb-528667146421-537045169862-VOMrlBMCHe4vPK1Gw7RbI3ki'

# instantiate Slack client
# slack_client = SlackClient('xoxb-528667146421-537045169862-VOMrlBMCHe4vPK1Gw7RbI3ki') #test workspace
slack_client = SlackClient('xoxb-2279330878-540470084641-lNJLvCQgq4kRWrSBEqAunyXl') # DPE workspace
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 2 # 1 second delay between reading from RTM
EXAMPLE_COMMAND = "burrito"
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

###########################
def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get('ok'):
        return channels_call['channels']
    return None

def list_users():
    users_call = slack_client.api_call("users.list")
    if users_call.get('ok'):
        return users_call['members']
    return None

def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            # print(event["text"])
            # print(message)
                
            if user_id == starterbot_id:
                # print(event["user"])
                return message, event["channel"], event["user"]
    return None, None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel, user_ask):
    
    """
        Executes bot command if the command is known
    """

    default_response = "To find out your burrito count, try *{}*".format(EXAMPLE_COMMAND)

    if command != EXAMPLE_COMMAND:
        slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=default_response
        )
    else:
        # csv_data, csv_array,csv_dict = extract("http://badburrito-env.ig82u4eta6.us-east-2.elasticbeanstalk.com/users")
        csv_dict = extract("http://badburrito-env.ig82u4eta6.us-east-2.elasticbeanstalk.com/users")
        # csv_data, csv_array,csv_dict = extract_raw()
        flag = 0
        
        try:
            user_info = slack_client.api_call("users.info",user =user_ask)
            if user_info:
                print("FOUND")
                # print(user_info["user"])
                name_found= user_info["user"]["profile"]["email"]
                # print(name_found)
                # print(csv_dict)
                try: 
                    burritos = csv_dict[name_found]
                except:
                    print("User not in the burrito database.")
                    flag = 1
            else: print("User not in the workspace.")        
        except:
            print("ERROR")
            flag = 1
    
    
        # Default response is help text for the user
        # default_response = "Not sure what you mean. Try *{}*.".format(EXAMPLE_COMMAND)
        # default_response = "To find out your burrito count, try *{}*".format(EXAMPLE_COMMAND)

        # Finds and executes the given command, filling in response
        response = None
        # This is where you start to implement more commands!
        # if command.startswith(EXAMPLE_COMMAND):
        if (flag):
            response = "You are not in the burrito database."
        else:
            response = ("*{}*, you have {} burritos! "+":burrito:"*burritos).format(user_info["user"]["profile"]["display_name"],burritos)

        # Sends the response back to the channel
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=response or default_response
        )

if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        # channels = list_channels()    
        # users = list_users()

        while True:
            command, channel, user_ask = parse_bot_commands(slack_client.rtm_read())
            # if command:
            handle_command(command, channel,user_ask)
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")